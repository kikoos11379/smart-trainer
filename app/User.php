<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    protected $fillable = [
        'name', 'email', 'password','age','weight','height','gender','activity','goal','goal_weight'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function calories(){
        return $this->hasOne(Calorie::class);
    }

    public function updateCalories(){

    }
    public function CreateMacros(User $user)
    {
        $m = $user->weight;
        $h = $user->height;
        $a = $user->age;
        $s = 5;
        if ($user->gender == 'f') {
            $s = -151;
        }
        $activity = $user->activity;

        $calories = round((((10 * $m + 6.25 * $h - 0.5 * $a) + $s) * $activity) + $user->goal);
        $protein = round((0.3 * $calories) / 4);
        $fat = round((0.2 * $calories) / 9);
        $carbs = round((0.5 * $calories) / 4);

        return $this->calories()->create([
            'user_id' => $user->id,
            'calories' => $calories,
            'protein' => $protein,
            'fat' => $fat,
            'carbs' => $carbs
        ]);
    }

}
