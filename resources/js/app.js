require('./bootstrap');

import App from './components/App'
import Vue from 'vue'
import store from './store/store'
import router from "./router/router"
import vuetify from "./vuetify/vuetify"
import axios from 'axios';

axios.defaults.baseURL = "http://localhost:8000/api/"

import mix from './mixins/mixin'

Vue.mixin(mix);


const app = new Vue({
    el: '#app',
    
    template: '<App/>',
    vuetify,
    store,
    router,

    components: { App }
});

