import Vue from 'vue'
import Router from 'vue-router'
import Home from "../components/Home"
import Login from "../components/Auth/Login"
import Register from "../components/Auth/Register"
import Diary from "../components/Diary/Diary"
import Workouts from "../components/Training/Workouts"
import Workout from "../components/Training/Workout";
import Progress from "../components/Progress/Progress";
import Friends from "../components/Friends/Friends";
import Friend from "../components/Friends/Friend";
import Blog from "../components/Blog/Blog";
import Post from "../components/Blog/Post";
import Settings from "../components/Settings/Settings";
import General from "../components/Settings/General";
import Account from "../components/Settings/Account";
import Goals from "../components/Settings/Goals";
import Notifications from "../components/Settings/Notifications";
import Privacy from "../components/Settings/Privacy";
import About from "../components/Settings/About";

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Login',
            component:Login,
           
        },
        {
            path: '/register',
            name: 'Register',
            component:Register,
           
        },
        {
            path: '/home',
            name: 'Home',
            component:Home,
           
        },
        {
            path: '/diary',
            name: 'Diary',
            component:Diary,
           
        },
        {
            path: '/training',
            name: 'Workouts',
            component:Workouts,

        },
        {
            path: '/training/workout/:id',
            name: 'Workout',
            props:true,
            component:Workout,

        },
        {
            path: '/progress',
            name: 'Progress',

            component:Progress,

        },
        {
            path: '/friends',
            name: 'Friends',

            component:Friends,

        },
        {
            path: '/friends/friend/:user_id',
            name: 'Friend',
            props:true,
            component:Friend,

        },
        {
            path: '/blog',
            name: 'Blog',
            component:Blog,

        },
        {
            path: '/blog/post/:post_id',
            name: 'Post',
            component:Post,

        },
        {
            path: '/settings/',
            name: 'Settings',
            component:Settings,
            children:[
                {
                    path:'/',
                    components:{
                        b:General
                    }
                },
                {
                    path:'account/',
                    components:{
                        b:Account
                    }
                },
                {
                    path:'goals/',
                    components:{
                        b:Goals
                    }
                },
                {
                    path:'notifications/',
                    components:{
                        b:Notifications
                    }
                },
                {
                    path:'privacy/',
                    components:{
                        b:Privacy
                    }
                },
                {
                    path:'about/',
                    components:{
                        b:About
                    }
                },
            ]

        },
    ]
})