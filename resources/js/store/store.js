import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
 const store = new Vuex.Store({
    state: {
        token:null,
    },
    mutations: {
        setToken (state,token) {
            state.token = token;
        },
        removeToken(state){
            this.state.token = null;
        }
    },
    getters:{
        getToken:state => {
            return state.token;
        }
    },

});
export default store;