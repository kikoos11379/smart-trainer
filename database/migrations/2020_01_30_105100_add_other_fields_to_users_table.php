<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOtherFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('age');
            $table->float('weight');
            $table->integer('height');
            $table->char('gender');
            $table->float('activity');
            $table->integer('goal');
            $table->float('goal_weight');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->removeColumn('age');
            $table->removeColumn('weight');
            $table->removeColumn('height');
            $table->removeColumn('gender');
            $table->removeColumn('activity');
            $table->removeColumn('goal');
            $table->removeColumn('goal_weight');
        });
    }
}
